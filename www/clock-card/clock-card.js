class ClockCard extends HTMLElement {
    set hass(hass) {
        if (!this.card) {
            this.card = document.createElement('ha-card');
            this.content = document.createElement('div');
            this.content.style.padding = '20px';
            this.content.style.textAlign = "center";
            this.content.className = "header ha-card";
            this.card.appendChild(this.content);
            this.appendChild(this.card);
        }

        this.content.innerHTML = hass.states['sensor.time'].state + "<br/>" + hass.states['sensor.date'].state;
    }

    setConfig(config) {
        this.config = config;
    }

    getCardSize() {
        return 1;
    }
}

customElements.define('clock-card', ClockCard);
