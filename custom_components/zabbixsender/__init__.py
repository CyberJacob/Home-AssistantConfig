"""Support for Zabbix metrics export."""
import logging
import string
import collections
import json

import voluptuous as vol

from homeassistant import core as hacore
from homeassistant.components.climate.const import ATTR_CURRENT_TEMPERATURE
from homeassistant.const import (
    ATTR_DEVICE_CLASS,
    ATTR_TEMPERATURE,
    ATTR_UNIT_OF_MEASUREMENT,
    CONTENT_TYPE_TEXT_PLAIN,
    EVENT_STATE_CHANGED,
    TEMP_CELSIUS,
    TEMP_FAHRENHEIT,
)
from homeassistant.helpers import entityfilter, state as state_helper
import homeassistant.helpers.config_validation as cv
from homeassistant.helpers.entity_values import EntityValues
from homeassistant.util.temperature import fahrenheit_to_celsius
import pyzabbix

_LOGGER = logging.getLogger(__name__)

DOMAIN = "zabbixsender"
CONF_SERVER = "server"
CONF_PORT = "port"
CONF_HOSTNAME = "hostname"

CONFIG_SCHEMA = vol.Schema(
    {
        DOMAIN: vol.All(
            {
                vol.Required(CONF_SERVER): cv.string,
                vol.Optional(CONF_PORT, default=10051): cv.string,
                vol.Required(CONF_HOSTNAME): cv.string,
            }
        )
    },
    extra=vol.ALLOW_EXTRA,
)


def setup(hass, config):
    """Activate Zabbixsender component."""

    conf = config[DOMAIN]
    climate_units = hass.config.units.temperature_unit

    sender = pyzabbix.sender.ZabbixSender(conf.get(CONF_SERVER), conf.get(CONF_PORT))

    metrics = ZabbixSender(
        sender,
        conf.get(CONF_HOSTNAME),
        climate_units
    )

    hass.bus.listen(EVENT_STATE_CHANGED, metrics.handle_event)
    return True


class ZabbixSender:
    """Model all of the metrics which should be exposed to Prometheus."""

    def __init__(
        self,
        sender,
        hostname,
        climate_units
    ):
        """Initialize Prometheus Metrics."""
        self._sensor_metric_handlers = [
            self._sensor_attribute_metric,
            self._sensor_fallback_metric,
        ]

        self._metrics = {}
        self._sender = sender
        self._hostname = hostname
        self._climate_units = climate_units

    def handle_event(self, event):
        """Listen for new messages on the bus, and add them to Prometheus."""
        state = event.data.get("new_state")
        if state is None:
            return

        entity_id = state.entity_id
        _LOGGER.debug("Handling state update for %s", entity_id)
        domain, _ = hacore.split_entity_id(entity_id)

        handler = f"_handle_{domain}"

        if hasattr(self, handler):
            getattr(self, handler)(state)
        else:
            _LOGGER.debug("No handler for domain %s", domain)

        #metric = self._metric(
        #    "state_change", "The number of state changes"
        #)
        #metric.attributes(**self._attributes(state)).inc()

    def _metric(self, entity_id, metric, value, attributes):
        key = self._sanitize_metric_name(entity_id + "." + metric)
        try:
            old_attributes = self._metrics[key]
            if attributes != old_attributes:
                self._metrics[key] = attributes
                #self._send_new_discovery()
        except KeyError:
            self._metrics[key] = attributes
            #self._send_new_discovery()

        #TODO: Data types
        _LOGGER.debug("reporting to Zabbix %s: %s", key, value)
        metric = pyzabbix.ZabbixMetric(self._hostname, key, value)
        #self._sender.send([metric])

    def _send_new_discovery(self):
        output = []

        for key in self._metrics:
            metric = {
                'name': key
            }

            for attribute in self._metrics['attributes']:
                metric[attribute] = self._metrics['attributes'][attribute]

            output.append(metric)

        _LOGGER.debug("reporting to Zabbix metrics: %s", output)
        metric = pyzabbix.ZabbixMetric(self._hostname, "discovery", json.dumps(output))
        #self._sender.send([metric])

    @staticmethod
    def _sanitize_metric_name(metric: str) -> str:
        return "".join(
            [
                c
                if c in string.ascii_letters or c.isdigit() or c == "_" or c == "_" or c == "."
                else f"u{hex(ord(c))}"
                for c in metric
            ]
        )

    @staticmethod
    def state_as_number(state):
        """Return a state casted to a float."""
        try:
            value = state_helper.state_as_number(state)
        except ValueError:
#            _LOGGER.warning("Could not convert %s to float", state)
            value = 0
        return value

    @staticmethod
    def _default_attributes(state):
        return {
            "entity": state.entity_id,
            "domain": state.domain,
            "friendly_name": state.attributes.get("friendly_name"),
        }

    def _battery(self, state):
        if "battery_level" in state.attributes:
            metric = self._metric(
                state.entity_id,
                "battery_level_percent",
                float(state.attributes["battery_level"]),
                {"description": "Battery level as a percentage of its capacity"}.update(self._default_attributes(state))
            )

    def _handle_binary_sensor(self, state):
        metric = self._metric(
            state.entity_id,
            "binary_sensor_state",
            self.state_as_number(state),
            {"description": "State of the binary sensor (0/1)"}.update(self._default_attributes(state))
        )

    def _handle_input_boolean(self, state):
        self._metric(
            state.entity_id,
            "input_boolean_state",
            self.state_as_number(state),
            {"description": "State of the input boolean (0/1)"}.update(self._default_attributes(state))
        )

    def _handle_device_tracker(self, state):
        metric = self._metric(
            state.entity_id,
            "device_tracker_state",
            self.state_as_number(state),
            {"description": "State of the device tracker (0/1)"}.update(self._default_attributes(state))
        )

    def _handle_person(self, state):
        metric = self._metric(
            state.entity_id,
            "person_state",
            self.state_as_number(state),
            {"description": "State of the person (0/1)"}.update(self._default_attributes(state))
        )

    def _handle_light(self, state):
        if "brightness" in state.attributes:
            self._metric(
                state.entity_id,
                "light_state",
                state.attributes["brightness"],
                {"description": "Load level of a light (0..255)"}.update(self._default_attributes(state))
            )
        else:
            self._metric(
                state.entity_id,
                "light_state",
                self.state_as_number(state),
                {"description": "Load level of a light (0..255)"}.update(self._default_attributes(state))
            )

    def _handle_lock(self, state):
        self._metric(
            state.entity_id,
            "lock_state",
            self.state_as_number(state),
            {"description": "State of the lock (0/1)"}.update(self._default_attributes(state))
        )

    def _handle_climate(self, state):
        temp = state.attributes.get(ATTR_TEMPERATURE)
        if temp:
            if self._climate_units == TEMP_FAHRENHEIT:
                temp = fahrenheit_to_celsius(temp)
            self._metric(
                state.entity_id,
                "temperature_c",
                temp,
                {"description": "Temperature in degrees Celsius"}.update(self._default_attributes(state))
            )

        current_temp = state.attributes.get(ATTR_CURRENT_TEMPERATURE)
        if current_temp:
            if self._climate_units == TEMP_FAHRENHEIT:
                current_temp = fahrenheit_to_celsius(current_temp)
            self._metric(
                state.entity_id,
                "current_temperature_c",
                current_temp,
                {"description": "Current Temperature in degrees Celsius"}.update(self._default_attributes(state))
            )

    def _handle_sensor(self, state):
        unit = self._unit_string(state.attributes.get(ATTR_UNIT_OF_MEASUREMENT))

        for metric_handler in self._sensor_metric_handlers:
            metric = metric_handler(state, unit)
            if metric is not None:
                break

        if metric is not None:
            value = self.state_as_number(state)
            if unit == TEMP_FAHRENHEIT:
                value = fahrenheit_to_celsius(value)

            self._metric(
                state.entity_id,
                "value",
                value,
                {"description": f"Sensor data measured in {unit}"}.update(self._default_attributes(state))
            )

        self._battery(state)

    @staticmethod
    def _sensor_attribute_metric(state, unit):
        """Get metric based on device class attribute."""
        metric = state.attributes.get(ATTR_DEVICE_CLASS)
        if metric is not None:
            return f"{metric}_{unit}"
        return None

    @staticmethod
    def _sensor_fallback_metric(state, unit):
        """Get metric from fallback logic for compatability."""
        if unit in (None, ""):
            _LOGGER.debug("Unsupported sensor: %s", state.entity_id)
            return None
        return f"sensor_unit_{unit}"

    @staticmethod
    def _unit_string(unit):
        """Get a formatted string of the unit."""
        if unit is None:
            return

        units = {
            TEMP_CELSIUS: "c",
            TEMP_FAHRENHEIT: "c",  # F should go into C metric
            "%": "percent",
        }
        default = unit.replace("/", "_per_")
        default = default.lower()
        return units.get(unit, default)

    def _handle_switch(self, state):
        metric = self._metric(
            state.entity_id,
            "switch_state",
            self.state_as_number(state),
            {"description": "State of the switch (0/1)"}.update(self._default_attributes(state))
        )

    def _handle_zwave(self, state):
        self._battery(state)

    def _handle_automation(self, state):
        metric = self._metric(
            state.entity_id,
            "automation_triggered_count",
            state.state,
            {"description": "Count of times an automation has been triggered"}.update(self._default_attributes(state))
        )
